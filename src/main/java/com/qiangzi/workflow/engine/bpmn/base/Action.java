package com.qiangzi.workflow.engine.bpmn.base;

public enum Action {

	INVOKE, IGNORE;
}
