package com.qiangzi.workflow.engine.bpmn.event;

import org.dom4j.Element;

import com.qiangzi.workflow.engine.bpmn.base.Node;

public abstract class Event extends Node {

	public void parse(Element element) throws Exception {
		super.parse(element);
	}

	protected void copy(Object obj) {
		super.copy(obj);
	}

}
